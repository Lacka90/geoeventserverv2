package app.authentication;

import app.user.User;
import app.user.UserService;
import com.google.gson.Gson;
import app.models.requests.LoginRQ;
import app.models.requests.LogoutRQ;
import app.utils.UserUtils;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

/**
 * Created by lacka on 27/08/15.
 */
@Path("auth")
public class AuthController {
    @Inject
    private AuthService authService;
    @Inject
    private SessionService sessionService;
    @Inject
    private UserService userService;
    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(LoginRQ req) {
        System.out.println(new Gson().toJson(req));
        String username = req.getUserName();
        String password = req.getPassword();
        User user = authenticate(username, password);

        UUID uuid = UUID.randomUUID();
        sessionService.set(uuid, user.getId());

        if(user != null) {
            return Response.status(200).entity(uuid.toString()).build();
        } else {
            return Response.status(404).entity(new AuthenticationError(username)).build();
        }
    }

    private User authenticate(String username, String password) {
        User user = null;

        if(username != null && password != null) {
            String salt = authService.getSalt(username);
            if (salt != null && salt.length() > 0) {
                String hash = UserUtils.calculateHash(password, salt);
                Long userId = authService.getUser(username, hash);
                if (userId != null) {
                    UUID uuid = UUID.randomUUID();
                    user = userService.get(userId);
                    sessionService.set(uuid, userId);
                }
            }
        }

        return user;
    }

    @POST
    @Path("/logout")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(LogoutRQ req) {
        String sessionId = req.getSessionId();
        if(sessionId != null) {
            sessionService.remove(UUID.fromString(sessionId));
            return Response.status(200).entity("Logout success").build();
        } else {
            return Response.status(404).entity("SessionID missing").build();
        }
    }
}
