package app.registration;

import app.user.User;
import app.models.requests.RegistrationRQ;
import app.authentication.SessionService;
import app.user.UserService;
import app.utils.UserUtils;
import com.google.gson.Gson;
import org.apache.commons.codec.binary.Base64;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import javax.inject.Inject;
import javax.persistence.PersistenceException;
import javax.transaction.*;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by lacka on 19/01/16.
 */
@Path("registration")
public class RegistrationController {
    String uploadDir = "/Users/lacka/Documents/avatars/";

    @Inject
    private UserService userService;
    @Inject
    private SessionService sessionService;

    @POST
    @Consumes("multipart/form-data")
    public Response register(MultipartFormDataInput multiPart) {
        Map<String, List<InputPart>> params = multiPart.getFormDataMap();
        Response ret = null;
        try {
            RegistrationRQ req = new Gson().fromJson(params.get("user").get(0).getBodyAsString(), RegistrationRQ.class);
            InputPart imagePart = getInputPart(params);

            String fileName = processImage(req.getUsername(), imagePart);

            User user = new User(req, UserUtils.generateHash(req.getPassword()));

            if (imagePart != null) {
                user.setAvatar(uploadDir + fileName);
            }
            userService.register(user);

            ret = Response.status(200).entity("Registration success").build();
        } catch (PersistenceException e) {
            ret = Response.status(403).entity("Username or email already taken").build();
        } catch (Exception e) {
            e.printStackTrace();
            ret = Response.status(500).entity(e.getMessage()).build();
        }

        return ret;
    }

    private InputPart getInputPart(Map<String, List<InputPart>> params) {
        InputPart imagePart = null;
        if (params.containsKey("image")) {
            imagePart = params.get("image").get(0);
        }
        return imagePart;
    }

    private String processImage(String userName, InputPart image) throws IOException, HeuristicRollbackException, HeuristicMixedException, NotSupportedException, RollbackException, SystemException {

        generateDirStructure(uploadDir);
        String fileName = "dummy.png";
        if (image != null) {
            fileName = generateFileName(userName, image);
            saveToDisk(image, uploadDir, fileName);
        }
        return fileName;
    }

    private void generateDirStructure(String uploadDir) {
        new File(uploadDir).mkdirs();
    }

    private String generateFileName(String userName, InputPart part) {
        return new String(Base64.encodeBase64(userName.getBytes())) + "." + part.getMediaType().getSubtype();
    }

    private void saveToDisk(InputPart part, String uploadDir, String fileName) throws IOException {
        InputStream inputStream = part.getBody(InputStream.class, null);
        FileOutputStream outputStream = new FileOutputStream(uploadDir + fileName);
        byte[] b = new byte[1024];
        int len;
        while ((len = inputStream.read(b)) != -1) {
            outputStream.write(b, 0, len);
        }
        outputStream.flush();
        outputStream.close();
        inputStream.close();
    }
}
