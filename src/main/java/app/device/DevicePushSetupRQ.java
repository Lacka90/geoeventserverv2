package app.device;

/**
 * Created by lacka on 24/01/16.
 */
public class DevicePushSetupRQ {
    private Long userId;
    private boolean push;

    public DevicePushSetupRQ() {}

    public DevicePushSetupRQ(Device device) {
        this.userId = device.getProfileID();
        this.push = device.getRecvPush();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public boolean isPush() {
        return push;
    }

    public void setPush(boolean push) {
        this.push = push;
    }
}
