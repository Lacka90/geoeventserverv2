package app.controllers;

import com.google.gson.Gson;
import app.models.Picture;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import app.services.PictureService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * Created by lacka on 06/05/15.
 */
@Path("/picture")
public class PictureController {
    public static final String UPLOAD_BASE = "/var/www/";
    public static final String PICTURE_FOLDER = ".upload/";
    public static final String HTTP_BASE = "http://devintra.hu:8888/";
    @Inject
    private PictureService pictureService;

    @GET
    public Response get(@QueryParam("id") String eventId) {
        System.out.println("Picture Event ID: " + eventId);
        Picture pic = pictureService.get(eventId);

        if (pic != null) {
            return Response.status(200).entity(pic.getFileData()).build();
        } else {
            return Response.status(500).entity("Picture not found").build();
        }
    }

    @POST
    @Path("/uploaddata")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response postWithData(Picture picture) {
        Response ret;
        try {
            pictureService.save(picture);
            ret = Response.status(200).entity(picture.getId()).build();
        } catch (Exception e) {
            e.printStackTrace();
            ret = Response.status(500).entity("Picture upload failed").build();
        }
        return ret;
    }

    @POST
    @Path("/uploadpicture")
    @Consumes("multipart/form-data")
    public Response postWithPhoto(MultipartFormDataInput multiPart) {
        Map<String, List<InputPart>> params = multiPart.getFormDataMap();
        Response ret;
        try {
            String imageId = params.get("id").get(0).getBodyAsString();
            Picture picture = pictureService.getAttachedPicture(Long.parseLong(imageId));
            InputPart imagePart = params.get("file").get(0);
            processImage(picture, imagePart);
            ret = Response.status(200).entity("Picture upload success").build();
        } catch (IOException e) {
            e.printStackTrace();
            ret = Response.status(500).entity("Picture upload failed").build();
        }

        return ret;
    }

    private void processImage(Picture picture, InputPart part) throws IOException {
        String fileName = picture.getEventID() + "_" + System.currentTimeMillis();
        saveToDisk(part, fileName);
        picture.setFileData(HTTP_BASE + PICTURE_FOLDER + fileName + ".jpg");
        save(picture);
    }

    private void saveToDisk(InputPart part, String fileName) throws IOException {
        InputStream inputStream = part.getBody(InputStream.class, null);
        FileOutputStream outputStream = new FileOutputStream(UPLOAD_BASE + PICTURE_FOLDER + fileName + ".jpg");
        byte[] b = new byte[1024];
        int len;
        while ((len = inputStream.read(b)) != -1) {
            outputStream.write(b, 0, len);
        }
        outputStream.flush();
        outputStream.close();
        inputStream.close();
    }

    private void save(Picture picture) {
        pictureService.merge(picture);
    }
}
