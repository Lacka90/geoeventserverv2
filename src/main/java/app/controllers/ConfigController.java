package app.controllers;

import com.google.gson.Gson;
import app.models.Config;
import app.services.ConfigService;

import javax.inject.Inject;
import javax.json.*;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by lacka on 06/05/15.
 */
@Path("/config")
public class ConfigController {
    @Inject
    private ConfigService configService;

    @GET
    public JsonArray get() {
        JsonArrayBuilder jab = Json.createArrayBuilder();
        for (Config c : configService.getAll()) {
            jab.add(Json.createObjectBuilder().add("config", c.getId()));
        }
        return jab.build();
    }

    @GET
    public String get(@PathParam("key") String key) {
        return new Gson().toJson(configService.get(key));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response add(JsonObject str) {
        System.out.println("asdasd" + str);
        Config cfg = new Gson().fromJson(str.toString(), Config.class);
        configService.add(cfg);
        return Response.status(200).entity(str).build();
    }
}
