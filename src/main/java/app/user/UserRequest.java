package app.user;

/**
 * Created by lacka on 27/08/15.
 */
public class UserRequest {
    String sessionId;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
