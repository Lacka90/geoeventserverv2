package app.models;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "config", uniqueConstraints =
@UniqueConstraint(columnNames = {"key_"}))
public class Config implements Serializable {

	private static final long serialVersionUID = -962600514103715356L;

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Long id;

	@Column(name = "key_")
	private String key;

	private String value;

	private String note;

	public Config() {
	}

	public Config(String key, String value, String note) {
		this.key = key;
		this.value = value;
		this.note = note;
	}

	public Long getId() {
		return id;
	}

	public String getKey() {
		return key;
	}

	public String getNote() {
		return note;
	}

	public String getValue() {
		return value;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
