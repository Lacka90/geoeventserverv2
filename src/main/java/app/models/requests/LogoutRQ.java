package app.models.requests;

/**
 * Created by lacka on 27/08/15.
 */
public class LogoutRQ {
    String sessionId;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
