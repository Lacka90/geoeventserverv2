package app.event.create;

import app.user.User;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "events", uniqueConstraints=
@UniqueConstraint(columnNames = {"appEventID"}))
public class Event implements Serializable {

	private static final long serialVersionUID = 1234781663102756327L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private Long eventTime;
	
	private Double gpsLong;

	private Double gpsLat;

	private String message;
	
	private String deviceId;

	private String appEventId;
	
	private String address;

	private String reward;

	@ManyToOne(targetEntity = User.class)
	@JoinColumn(name = "userId", referencedColumnName="id")
	private User user;

	public Event() {}

	public Event(Long eventTime, Double gpsLong, Double gpsLat,
			String message, String deviceId, String appEventId,
			String address, String reward, User user) {
		this.eventTime = eventTime;
		this.gpsLong = gpsLong;
		this.gpsLat = gpsLat;
		this.message = message;
		this.deviceId = deviceId;
		this.appEventId = appEventId;
		this.reward = reward;
		this.user = user;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getEventTime() {
		return eventTime;
	}

	public void setEventTime(Long eventTime) {
		this.eventTime = eventTime;
	}

	public Double getGpsLong() {
		return gpsLong;
	}

	public void setGPSLong(Double gpsLong) {
		gpsLong = gpsLong;
	}

	public Double getGpsLat() {
		return gpsLat;
	}

	public void setGpsLat(Double gPSLat) {
		gpsLat = gPSLat;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getAppEventId() {
		return appEventId;
	}

	public void setAppEventId(String appEventId) {
		this.appEventId = appEventId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getReward() {
		return reward;
	}
	
	public void setReward(String reward) {
		this.reward = reward;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void update(Event event) {
		this.id = event.getId();
		this.eventTime = event.getEventTime();
		this.gpsLong = event.getGpsLong();
		this.gpsLat = event.getGpsLat();
		this.message = event.getMessage();
		this.deviceId = event.getDeviceId();
		this.appEventId = event.getAppEventId();
		this.reward = event.getReward();
		this.user = event.getUser();
	}
	
}
