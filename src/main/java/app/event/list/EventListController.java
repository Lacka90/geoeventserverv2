package app.event.list;

import app.event.create.EventService;
import app.event.respond.Respond;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by lacka on 25/01/16.
 */
@Path("/events")
public class EventListController {
    @Inject
    private EventService eventService;

    @GET
    @Path("/list/{sort}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBySort(@PathParam("sort") String sort) {
        Events events = eventService.getEvents(sort);
        return Response.status(200).entity(events).build();
    }
}
