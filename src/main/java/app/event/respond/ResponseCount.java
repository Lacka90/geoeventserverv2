package app.event.respond;

import java.io.Serializable;

/**
 * Created by lacka on 31/01/16.
 */
public class ResponseCount implements Serializable {
    private int count;

    public ResponseCount() {}

    public ResponseCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
