package app;

import javax.annotation.PostConstruct;
import javax.ws.rs.ApplicationPath;

/**
 * Created by lacka on 06/05/15.
 */
@ApplicationPath("/api/v1")
public class Application extends javax.ws.rs.core.Application {}
