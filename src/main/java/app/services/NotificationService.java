package app.services;

import app.models.NotifiedFromCreateEvent;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by lacka on 06/05/15.
 */
@Stateless
public class NotificationService {
    @PersistenceContext
    private EntityManager em;

    public boolean saveCreateNotif(NotifiedFromCreateEvent notif) {
        boolean ret = true;
        try {
            em.persist(notif);
        } catch (Exception e){
            ret = false;
        }
        return ret;
    }
}
